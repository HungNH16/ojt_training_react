# Class
* class được định nghĩa trong es2015 là 1 sugar syntax cho việc viết function
* không hoiting
```js
function dnd (data){
    this.data = data
}
 dnd.prototype.getData = function(){
     console.log(this.data)
 }
 dnd.prototype.setData = function(data){
     this.data = data;
 }
class dnd{
    constructor(data){
        this.data = data
    }
    getData(){
        console.log(this.data)
    }
    setData(data){
        this.data = data
    }
    get dnd(){
        return this.data
    }
}
```
# prototype trong class
```js
class o1 {
    constructor(...data) {
        this.a = data[0]
    }
    dnd() {
        console.log("dnd", this.a)
        return 22
    }
}
var o2 = new o1(2);
console.log(o2.__proto__ === o1.prototype); // true
console.log(o2)
class o3 extends o1 {//
    constructor(num) {
        super(num)
    }
    dnd() {
        console.log(this.a)
    }
}
const o4 = new o3(4)
console.log(o3.__proto__ === o1) //true
o4.dnd() // 4
```
* Trình tự thực hiện khi khỏi tao instance bằng toàn tử new
* 1 tao object o1
* 2 gán this = o1
* 3 return this
* 4 o1.prototype = Object;
* 5 sau đó là gán biến o2 = o1: // o2 = this // gán giá trị ???? hay gán tham chiếu 
* 6 khi này o2.proto = o1.prototype = Object;
* 7 o2 === o1 // false

khi sử dụng extends <=> o3 = Object.create(o1);

(o3.__proto__ === o1) //true
* khởi tạo object o3 
* gán o3.__proto__ = o1
# constructor
* khởi tạo object được sinh ra cùng class
* dùng để khởi tạo các attribute cho class
* nới nhận cấc tham số truyền vào
* có thể dùng super() method use for call contructor của class cha. Mặc định gọi
* => trong react luôn gọi vs tham số đầu vào là props
# static
* định nghĩa các phương thưc static: ko thừa kế, chỉ thằng khởi tạo mới gọi
* sử dụng để tạo các phương thức dùng chung của class
# State And Lifecycle
* Là cách để biến 1 component thực sự độc lập và có khả năng tái sử dụng
* state là trạng thái của riêng component
```js
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}

ReactDOM.render(
  <Clock />,
  document.getElementById('root')
);
```
* mô tả:

b1:  ReactDOM.render() gọi  => react gọi contructor => khai báo time trong state

b2: Sau đó gọi hàm render() của Component Clock => báo cho react biết cần hiển thị cái gì => react update DOM cho phù hợp

b3: khi mà Clock đã đc chèn vào DOM ( đã hiển thị rồi ) thì react mới gọi componentDidMount() lifecycle hook. Component yêu cầu trình duyệt set up timer gọi hàm tick()

b4: mỗi lần gọi hàm stick Component gọi hàm setState() báo cho React biết là state sẽ thay đồi và gọi hàm render() lần nữa để hiển thị
b5: gọi hàm componentWillMount() để dừng hàm timer
# Using State Correctly
* không thay đổi state trực tiếp mà gọi hàm setState()
* Việc update() có thể bất đồng bộ để tăng hiệu năng => khắc phục bằng cách gọi hàm bất đồ bộ 
