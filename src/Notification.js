import ReactDOM from 'react-dom';
import React, { Component } from 'react';

const Notification = (props) => {
    const { title } = props;
    return (
      <div className="notification">
        <h2 className="message__title">{title}</h2>
        <p>{props.children}</p>
      </div>
    );
  };
  export default Notification;