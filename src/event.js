import React from 'react';
class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data:'Du lieu ban dau...'
        }
        this.updateState = this.updateState.bind(this);
    };
    updateState() {
        this.setState({data: 'update du lieu...'})
     }
     render(){
         return (
             <div>
                <button onClick = {this.updateState}>Click me</button>
                <h4>{this.state.data}</h4>
             </div>
         );
     }
}
export default App;